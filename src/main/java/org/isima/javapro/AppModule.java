package org.isima.javapro;

import com.google.inject.AbstractModule;
import org.isima.javapro.services.LoggerService;
import org.isima.javapro.services.LoggerServiceImpl;
import org.isima.javapro.services.ProductService;
import org.isima.javapro.services.ProductServiceImpl;

public class AppModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(ProductService.class).to(ProductServiceImpl.class);

        bind(LoggerService.class).to(LoggerServiceImpl.class);
    }
}
