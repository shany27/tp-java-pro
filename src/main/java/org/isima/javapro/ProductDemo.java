package org.isima.javapro;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.isima.javapro.entities.Product;
import org.isima.javapro.handler.LoggerHandler;
import org.isima.javapro.handler.ProductHandler;

public class ProductDemo {

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new AppModule());
        ProductHandler productHandler = injector.getInstance(ProductHandler.class);
        Product product = productHandler.launch();

        LoggerHandler loggerHandler = injector.getInstance(LoggerHandler.class);
        loggerHandler.loggerLaunch(product);
    }
}
