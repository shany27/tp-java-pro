package org.isima.javapro.controllers;

import com.google.inject.Inject;
import org.isima.javapro.services.LoggerService;
import org.isima.javapro.entities.Product;

public class LoggerController {
    private LoggerService iLogger;

    @Inject
    public LoggerController(LoggerService iLogger) {
        this.iLogger = iLogger;
    }

    public void info(Product product){
        iLogger.saveInLog(product);
    }
}
