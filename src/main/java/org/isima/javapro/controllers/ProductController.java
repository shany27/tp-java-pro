package org.isima.javapro.controllers;

import com.google.inject.Inject;
import org.isima.javapro.entities.Product;
import org.isima.javapro.services.ProductService;


public class ProductController {
    private ProductService productService;

    @Inject
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    public Product makeRequest(String code){
        return productService.getDataFromAPI(code);
    }

    public int scoreRequest(Product product){
        return productService.nutriScore(product);
    }

    public String colorRequest(int score){
        return productService.colorProduct(score);
    }

    public String classeRequest(int score){
        return productService.classeProduct(score);
    }
}
