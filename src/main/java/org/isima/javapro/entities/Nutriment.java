package org.isima.javapro.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Nutriment implements Serializable {
    private int energy_100g;
    private double saturated_fat_100g;
    private double sugars_100g;
    private double salt_100g;
    private double fiber_100g;
    private double proteins_100g;
}
