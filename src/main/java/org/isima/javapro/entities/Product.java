package org.isima.javapro.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * Classe pour définir les éléments d'un produit
 */
@Data
@AllArgsConstructor
public class Product implements Serializable {

    private String code;
    private String name;
    private String quantity;
    private String classe;
    private String color;
    private int score;
    private Nutriment nutriment;

    public Product() {
        nutriment = new Nutriment();
    }

}
