package org.isima.javapro.handler;

import org.isima.javapro.controllers.LoggerController;
import org.isima.javapro.entities.Product;

import javax.inject.Inject;

public class LoggerHandler {

    private final LoggerController loggerController;

    @Inject
    public LoggerHandler(LoggerController loggerController) {
        this.loggerController = loggerController;
    }

    public void loggerLaunch(Product product){
        loggerController.info(product);
    }
}
