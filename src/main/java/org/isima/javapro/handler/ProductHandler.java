package org.isima.javapro.handler;

import com.google.inject.Inject;
import org.isima.javapro.controllers.ProductController;
import org.isima.javapro.entities.Product;

import java.util.Scanner;

public class ProductHandler {
    private final ProductController productController;

    @Inject
    public ProductHandler(ProductController productController) {
        this.productController = productController;
    }

    public Product getDataLaunch(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Entrez le code du produit");
        String code = sc.next();
        return productController.makeRequest(code);
    }

    public String colorProductLaunch(Product product){
        return productController.colorRequest(product.getScore());
    }

    public String classeProductLaunch(Product product){
        return productController.classeRequest(product.getScore());
    }

    public int nutriScoreLaunch(Product product){
        return productController.scoreRequest(product);
    }

    public Product launch(){
        Product product = getDataLaunch();
        product.setScore(nutriScoreLaunch(product));
        product.setColor(colorProductLaunch(product));
        product.setClasse(classeProductLaunch(product));
        return product;
    }
}
