package org.isima.javapro.services;

import org.isima.javapro.entities.Product;

public interface LoggerService {
    void saveInLog(Product product);
}
