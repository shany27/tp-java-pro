package org.isima.javapro.services;

import org.isima.javapro.entities.Product;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LoggerServiceImpl implements LoggerService {
    @Override
    public void saveInLog(Product product) {
        Logger logger = Logger.getLogger("MyLogger");
        FileHandler fileHandler = null;
        try {
            fileHandler = new FileHandler("MyLogFile.txt", 10000000, 1, true);
            fileHandler.setFormatter(new SimpleFormatter());
            logger.addHandler(fileHandler);
            logger.log(Level.INFO, "{" + "\n"
                    + "\t" + "Code : " + product.getCode() + "\n"
                    + "\t" + "Nom : " + product.getName() + "\n"
                    + "\t" + "Quantité : " + product.getQuantity() + "\n"
                    + "\t" + "Score nutritionnel : " + product.getScore() + "\n"
                    + "\t" + "Classe : " + product.getClasse() + "\n"
                    + "\t" + "Couleur : " + product.getColor() + "\n"
                    + "}");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
