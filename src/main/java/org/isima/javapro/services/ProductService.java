package org.isima.javapro.services;

import org.isima.javapro.entities.Product;

public interface ProductService {
    Product getDataFromAPI(String code);
    int nutriScore(Product product);
    String colorProduct(int score);
    String classeProduct(int score);
}
