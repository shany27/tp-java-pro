package org.isima.javapro.services;

import com.mashape.unirest.http.Unirest;
import org.isima.javapro.entities.Product;
import org.json.JSONObject;
import org.apache.http.ssl.SSLContexts;

public class ProductServiceImpl implements ProductService {

    @Override
    public Product getDataFromAPI(String code) {
        Product product = new Product();

        try{
            JSONObject requestResponse = Unirest.get("https://world.openfoodfacts.org/api/v0/product/" + code + ".json").asJson().getBody().getObject();
            JSONObject object = requestResponse.getJSONObject("product").getJSONObject("nutriments");

            product.setCode(requestResponse.getJSONObject("product").getString("id"));
            product.setName(requestResponse.getJSONObject("product").getString("product_name"));
            product.setQuantity(requestResponse.getJSONObject("product").getString("product_quantity"));

            product.getNutriment().setEnergy_100g(object.has("energy_100g") ? object.getInt("energy_100g") : 0);
            product.getNutriment().setSaturated_fat_100g(object.has("saturated-fat_100g") ? object.getDouble("saturated-fat_100g") : 0);
            product.getNutriment().setSugars_100g(object.has("sugars_100g") ? object.getDouble("sugars_100g") : 0);
            product.getNutriment().setSalt_100g(object.has("salt_100g") ? object.getDouble("salt_100g") : 0);
            product.getNutriment().setFiber_100g(object.has("fiber_100g") ? object.getDouble("fiber_100g") : 0);
            product.getNutriment().setProteins_100g(object.has("proteins_100g") ? object.getDouble("proteins_100g") : 0);

        }catch (Exception e){
            e.printStackTrace();
        }

        return product;
    }

    @Override
    public int nutriScore(Product product) {

        int score;

        //composante négative
        int negativeElement = 0;

        if (product.getNutriment().getEnergy_100g() > 335){
            negativeElement += (product.getNutriment().getEnergy_100g() < 3350 ? (product.getNutriment().getEnergy_100g()/335) : 10);
        }

        if (product.getNutriment().getSaturated_fat_100g() > 1){
            negativeElement += (product.getNutriment().getSaturated_fat_100g() < 10 ? product.getNutriment().getSaturated_fat_100g() : 10);
        }

        if (product.getNutriment().getSugars_100g() > 4.5){
            negativeElement += (product.getNutriment().getSugars_100g() < 45 ? (product.getNutriment().getSugars_100g()/4.5) : 10);
        }

        if (product.getNutriment().getSalt_100g() > 0.09){
            negativeElement += (product.getNutriment().getSalt_100g() < 0.9 ? (product.getNutriment().getSalt_100g()/0.09) : 10);
        }


        //composante positive
        int positiveElement = 0;

        if (product.getNutriment().getFiber_100g() > 0.9){
            positiveElement += (product.getNutriment().getFiber_100g() < 4.7 ? (product.getNutriment().getFiber_100g()/0.9) : 5);
        }

        if (product.getNutriment().getProteins_100g() > 1.6){
            positiveElement += (product.getNutriment().getProteins_100g() < 8 ? (product.getNutriment().getProteins_100g()/1.6) : 5);
        }

        score = negativeElement - positiveElement;
        
        return score;
    }

    @Override
    public String colorProduct(int score) {
        String color;
        if (score < 0) {
            color = "Green";
        } else if (score < 3) {
            color = "Light green";
        } else if (score < 11) {
            color = "Yellow";
        } else if (score < 19) {
            color = "Orange";
        } else {
            color = "Red";
        }
        return color;
    }

    @Override
    public String classeProduct(int score) {
        String classe;
        if (score < 0) {
            classe = "Trop Bon";
        } else if (score < 3) {
            classe = "Bon";
        } else if (score < 11) {
            classe = "Mangeable";
        } else if (score < 19) {
            classe = "Mouai";
        } else {
            classe = "Degueu";
        }
        return classe;
    }
}
