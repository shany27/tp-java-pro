package org.isima.javapro.controllerTest;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.isima.javapro.controllers.ProductController;
import org.isima.javapro.entities.Product;
import org.isima.javapro.serviceTest.ProductServiceTest;
import org.isima.javapro.services.ProductService;
import org.junit.After;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ProductControllerTest {

    private Injector injector;
    private Product product;
    String code = "7622210449283";

    @BeforeEach
    public void setUp(){
        injector = Guice.createInjector(new AbstractModule(){
            @Override
            protected void configure() {
                bind(ProductService.class).to(ProductServiceTest.class);
            }
        });
    }

    @After
    public void tearDown(){
        injector = null;
    }

    @Test
    public void getProductFromAPITest(){
        ProductController productController = injector.getInstance(ProductController.class);
        product = productController.makeRequest(code);
        Assert.assertEquals("123", product.getCode());
    }

    @Test
    public void getProductFromAPITestNotFound(){
        ProductController productController = injector.getInstance(ProductController.class);
        product = productController.makeRequest(code);
        Assert.assertNotEquals("12", product.getCode());
    }

    @Test
    public void getTestScore(){
        ProductController productRequest = injector.getInstance(ProductController.class);
        product = productRequest.makeRequest(code);
        product.setScore(productRequest.scoreRequest(product));
        Assert.assertEquals(15, product.getScore());
    }

    @Test
    public void getTestScoreNotFound(){
        ProductController productRequest = injector.getInstance(ProductController.class);
        product = productRequest.makeRequest(code);
        product.setScore(productRequest.scoreRequest(product));
        Assert.assertNotEquals(10, product.getScore());
    }

    @Test
    public void getTestColor(){
        ProductController productRequest = injector.getInstance(ProductController.class);
        product = productRequest.makeRequest(code);
        product.setColor(productRequest.colorRequest(productRequest.scoreRequest(product)));
        Assert.assertEquals("Orange", product.getColor());
    }

    @Test
    public void getTestColorNotFound(){
        ProductController productRequest = injector.getInstance(ProductController.class);
        product = productRequest.makeRequest(code);
        product.setColor(productRequest.colorRequest(productRequest.scoreRequest(product)));
        Assert.assertNotEquals("Light green", product.getColor());
    }

    @Test
    public void getTestClasse(){
        ProductController productRequest = injector.getInstance(ProductController.class);
        product = productRequest.makeRequest(code);
        product.setClasse(productRequest.classeRequest(productRequest.scoreRequest(product)));
        Assert.assertEquals("Mouai", product.getClasse());
    }

    @Test
    public void getTestClasseNotFound(){
        ProductController productRequest = injector.getInstance(ProductController.class);
        product = productRequest.makeRequest(code);
        product.setClasse(productRequest.classeRequest(productRequest.scoreRequest(product)));
        Assert.assertNotEquals("Mangeable", product.getClasse());
    }

}
