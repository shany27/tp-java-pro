package org.isima.javapro.serviceTest;

import org.isima.javapro.entities.Nutriment;
import org.isima.javapro.entities.Product;
import org.isima.javapro.services.ProductService;

public class ProductServiceTest implements ProductService {
    @Override
    public Product getDataFromAPI(String code) {
        Nutriment nutriment = new Nutriment(271, 0.1, 12, 0.90, 1.4, 1.4);
        return new Product("123", "Ketchup", "435", null, null, 0, nutriment);
    }

    @Override
    public int nutriScore(Product product) {
        return 15;
    }

    @Override
    public String colorProduct(int score) {
        return "Orange";
    }

    @Override
    public String classeProduct(int score) {
        return "Mouai";
    }
}
